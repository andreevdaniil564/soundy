import React, {useEffect} from 'react';
import {Route, Redirect, useLocation, useHistory} from "react-router-dom";

const ProtectedRoute = ({component: Component, ...rest}) => {
    return (
        <Route {...rest} render={props => {
            if (localStorage.getItem('userId')) {
                return <Component/>
            } else {
                return <Redirect to="/login" />
            }
        }}/>
    );
};

export default ProtectedRoute;

import React, {useEffect, useRef, useState} from 'react'
import { List, Avatar } from 'antd';
import axios from 'axios';
import {useLocation, useParams} from "react-router-dom";
import '../../../assets/css/upload.scss'
import '../../../assets/css/detail.scss'
import Comments from './Sections/Comments'
import moment from "moment";
import GetAudio from "../GetAudio/GetAudio";
import AudioPlayer from "../AudioPlayer";
import { MusicalNoteOutline, Heart, HeartOutline } from 'react-ionicons'
import NotFound from "../NotFound/NotFound";
import Spinner from 'react-spinner-material';

export default function DetailAudioPage ({mode}) {
    const [audio, setAudio] = useState(null)
    const [isLiked, setIsLiked] = useState(false)
    const [isLoading, setIsLoading] = useState(true)
    const [CommentLists, setCommentLists] = useState([])
    const [likesCount, setLikesCount] = useState(0)
    const [notFound, setNotFound] = useState(false)
    const location = useLocation();
    const userId = localStorage.getItem('userId')

    const audioVariable = {
        audioId: useParams().id
    }

    const secretLinkVariable = {
        secretLink: useParams().secretLink
    }

    const secret = {
        secret: useParams().secret
    }

    const handleAudio = (e) => {
        console.log(e)
    }

    const like = () => {
        axios.post('/api/audio/like', {isAudio: true, userId, audioId: audioVariable.audioId})
            .then(response => {
                if (response.data.success) {
                    setIsLiked(true)
                    setLikesCount(likesCount + 1)
                } else {
                    alert('Failed to get audio Info')
                }
            })
    }

    const unlike = () => {
        axios.post('/api/audio/unlike', {isAudio: true, userId,  audioId: audioVariable.audioId})
            .then(response => {
                if (response.data.success) {
                    setIsLiked(false)
                    setLikesCount(likesCount - 1)
                } else {
                    alert('Failed to get audio Info')
                }
            })
    }

    useEffect(() => {
        setIsLoading(true)
        if (secret.secret === undefined) {
            axios.post('/api/audio/get-specific', {audioId: audioVariable.audioId, })
                .then(response => {
                    if (response.data.success) {
                        setAudio(response.data.audio)
                        setIsLoading(false)
                        setNotFound(false)
                    } else {
                        alert('Failed to get audio Info')
                        setNotFound(true)
                    }
                })
        } else if (secret.secret === 'secret') {
            axios.post('/api/audio/get/secret-link', secretLinkVariable)
                .then(response => {
                    if (response.data.success) {
                        setAudio(response.data.audio)
                    } else {
                        alert('Failed to get audio Info')
                    }
                })
        }

        axios.post('/api/comment/getComments', audioVariable)
            .then(response => {
                if (response.data.success) {
                    setCommentLists(response.data.comments)
                } else {
                    alert('Failed to get audio Info')
                }
            })
        axios.post('/api/audio/add-listener', {audioId: audioVariable.audioId, userId})
            .then(response => {
                if (response.data.success) {
                    console.log('Added')
                } else {
                    alert('Failed')
                }
            })
        axios.post('/api/audio/get-like', {userId, audioId: audioVariable.audioId, isAudio: true})
            .then(response => {
                if (response.data.success) {
                    setIsLiked(response.data.like)
                }
            })
        axios.post('/api/audio/get-like-count', {audioId: audioVariable.audioId, isAudio: true})
            .then(response => {
                if (response.data.success) {
                    console.log(response.data.likesCount)
                    setLikesCount(response.data.likesCount == null ? 0 : response.data.likesCount)
                }
            })

    }, [location])

    const updateComment = (newComment) => {
        setCommentLists(CommentLists.concat(newComment))
    }

    return (
            <div className="post-page">
                {notFound ? <NotFound/> : ''}
                {audio != null ? <div><div className="audio-card audio" style={{margin: '5vh 0', width: '60vw'}}>
                    <div className="audio-title">{audio.title}</div>
                    {<AudioPlayer autoPlay={true} audio={audio}/>}
                    <div className="info">
                        <div className="like">
                            {isLiked ? <Heart onClick={() => unlike()}/> :
                                <HeartOutline onClick={() => like()}/>} {likesCount}
                        </div>
                        <div className="user">
                            {audio.writer ? <Avatar src={audio.writer.image}/> : ''}
                            {audio.writer ? audio.writer.name : ''}
                            {audio.group ? <Avatar src={audio.group.image}/> : ''}
                            {audio.group ? audio.group.name : ''}
                        </div>
                        <div><MusicalNoteOutline/> {audio.listened} listened</div>
                        <div className="date"> {moment(audio.createdAt).format("MMM Do YYYY")} </div>
                    </div>
                </div>

                <div style={{marginTop: isLoading ? 100 : '', width: '60vw'}}>
                    <GetAudio/>
                </div>
                <Comments CommentLists={CommentLists} postId={audio._id} refreshFunction={updateComment}/>
            </div>: <div style={{marginTop: 100}}><Spinner/></div>}
            </div>
        )
}

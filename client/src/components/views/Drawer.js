import React, {useState} from 'react';
import {useHistory, useLocation} from "react-router-dom";
import {Link} from "react-router-dom";
import "../../assets/css/drawer.scss"
import axios from "axios";
import {USER_SERVER} from "../Config";

const Drawer = (props) => {
    const path = useLocation().pathname
    const history = useHistory()

    const renderOptions = props.accountOptions.map((option) => {
        const mini = props.mode == 'mini' ? 'mini' : ''
        const active = path.includes(option.link.split('/')[1])
        return (
            <li className={`${mini} ${active ? 'active' : ''}`}
                style={{
                    background: active && props.theme === 'dark' ? '#303030' : ''
                }}
            >
                <Link to={option.link} className={`${active ? '' : 'inactive'}`}>
                    {active ? option.activeIcon : option.icon }
                    {props.mode == 'maxi' ? <span>{option.name}</span> : ''}
                </Link>
            </li>
        )
    })

    const logoutHandler = () => {
        localStorage.removeItem('userId')
        localStorage.removeItem('mode')
        window.location.reload()
        history.push("/login");
        axios.get(`${USER_SERVER}/logout`).then(res => {
            if (res.body.success) {
                console.log('Successfully signed out')
            }
        })
    };


    return (
        <div className="drawer">
            <ul className='drawer-list' style={{
                width: props.mode == 'mini' ? '' : '250px',
                background: props.theme === 'light' ? '#fafafa' : 'rgb(16, 20, 22)'
            }}>
                {renderOptions}
                {props.mode == 'maxi' ?
                    <button className="logout" onClick={() => logoutHandler()}>
                    Logout
                </button> : ''}
            </ul>
        </div>
    );
};

export default Drawer;

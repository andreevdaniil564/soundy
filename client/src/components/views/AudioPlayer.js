import '../../assets/css/sound-container.scss'
import React, {useEffect, useRef, useState} from 'react';
import Slider from "./Slider/Slider";
import ControlPanel from "./Controls/Controls";
import { VolumeMedium as VolumeDownIcon, VolumeHigh as VolumeUpIcon, VolumeMute as VolumeOffIcon, Settings as TuneIcon, ArrowBackOutline } from 'react-ionicons'
import {useLocation} from "react-router";
import axios from "axios";

const speedList = [
    {name: '0.25', value: 0.25},
    {name: '0.5', value: 0.5},
    {name: '0.75', value: 0.75},
    {name: '1', value: 1},
    {name: '1.25', value: 1.25},
    {name: '1.5', value: 1.5},
    {name: '1.75', value: 1.75},
    {name: '2', value: 2},
]

const AudioPlayer = (props) => {
    const audioRef = useRef()
    const [percentage, setPercentage] = useState(0)
    const [percentageVolume, setPercentageVolume] = useState(localStorage.getItem('volume') * 100)
    const [isPlaying, setIsPlaying] = useState(false)
    const [duration, setDuration] = useState(0)
    const [currentTime, setCurrentTime] = useState(0)
    const [wasSound, setWasSound] = useState(0)
    const [settingsOpened, setSettingsOpened] = useState(false)
    const [speedOpened, setSpeedOpened] = useState(false)
    const location = useLocation()

    const getCurrDuration = (e) => {
        const percent = ((e.currentTarget.currentTime / e.currentTarget.duration) * 100).toFixed(2)
        const time = e.currentTarget.currentTime

        setPercentage(+percent)
        setCurrentTime(time.toFixed(2))
    }

    const onChange = (e) => {
        const audio = audioRef.current
        audio.currentTime = (audio.duration / 100) * e.target.value
        setPercentage(e.target.value)
    }

    useEffect(() => {
        axios.post('/api/audio/add-time', {audioId: props.audio._id, currentTime})
            .then(response => {
                if (response.data.success) {
                    console.log('Added')
                } else {
                    alert('Failed')
                }
            })
    }, [location])

    const onChangeVolume = (e) => {
        const audio = audioRef.current
        audio.volume = e.target.value / 100

        localStorage.setItem('volume', audio.volume)
        setPercentageVolume(e.target.value)
    }


    const play = () => {
        const audio = audioRef.current
        audio.volume = localStorage.getItem('volume') ? localStorage.getItem('volume') : 1
        audio.playbackRate = localStorage.getItem('playbackRate') ? localStorage.getItem('playbackRate') : 1

        if (!isPlaying) {
            setIsPlaying(true)
            audio.play()
        }

        if (isPlaying) {
            setIsPlaying(false)
            audio.pause()
        }
    }

    const startPlaying = () => {
        setIsPlaying(true)
        const audio = audioRef.current
        audio.volume = localStorage.getItem('volume') ? localStorage.getItem('volume') : 1
        audio.playbackRate = localStorage.getItem('playbackRate') ? localStorage.getItem('playbackRate') : 1
    }

    const turnSound = () => {
        const audio = audioRef.current

        if (localStorage.getItem('volume') != 0) {
            localStorage.setItem('wasSound', localStorage.getItem('volume'))
            localStorage.setItem('volume', 0)
            audio.volume = 0
            setPercentageVolume(0)
        } else {
            localStorage.setItem('volume', localStorage.getItem('wasSound'))
            localStorage.setItem('wasSound', 0)
            audio.volume = localStorage.getItem('volume')
            setPercentageVolume(audio.volume * 100)
        }
    }

    const changeSpeed = (speed) => {
        const audio = audioRef.current
        localStorage.setItem('playbackRate', speed)
        audio.playbackRate = speed
        setSettingsOpened(false)
        setSpeedOpened(false)
    }

    const toggleSettings = () => {
        setSettingsOpened(!settingsOpened)
        setSpeedOpened(settingsOpened ? false : '')
    }

    const toggleSpeed = () => {
        setSpeedOpened(!speedOpened)
    }

    const speedsList = speedList.map(speed => {
        return (
            <li>
                <button onClick={() => changeSpeed(speed.value)}>{speed.name}</button>
            </li>
        )
    })

    return (
        <>
            <audio
                autoPlay={props.autoPlay}
                id="audio"
                onPlay={() => startPlaying()}
                ref={audioRef}
                onTimeUpdate={getCurrDuration}
                src={props.audio.filePath}
                onLoadedData={(e) => {
                    setDuration(e.currentTarget.duration.toFixed(2))
                }}
                />
                <div className="slider">
                    <Slider percentage={percentage} onChange={onChange}/>
                </div>
            <ControlPanel
                play={play}
                isPlaying={isPlaying}
                duration={duration}
                currentTime={currentTime}
            />
            <div className="container">
                <div className="sound">
                    {percentageVolume < 50 && percentageVolume != 0 ?
                        <VolumeDownIcon onClick={() => turnSound()} alt="" /> : ''}
                    {percentageVolume == 0 ?
                        <VolumeOffIcon onClick={() => turnSound()} alt="" />
                        : percentageVolume > 50 ? <VolumeUpIcon onClick={() => turnSound()} alt=""/> : ''}
                     <Slider percentage={percentageVolume} onChange={onChangeVolume} volume={true}/>
                </div>
                <TuneIcon onClick={() => {toggleSettings()}} alt="" style={{float: 'right', fill: '#9f9f9f'}}/>
                {settingsOpened && !speedOpened ? <ul className="settings">
                    <li>
                        {speedOpened ? '' :
                            <button onClick={() => toggleSpeed()}>Speed</button>
                        }
                    </li>
                </ul> : ''}
                {speedOpened ? <ul className="option">
                    <li>
                        <button onClick={() => toggleSpeed()}>
                            <ArrowBackOutline />Previous
                        </button>
                    </li>
                    {speedsList}
                </ul> : ''}
            </div>
        </>
    );
};

export default AudioPlayer;

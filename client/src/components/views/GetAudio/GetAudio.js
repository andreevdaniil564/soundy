import '../../../assets/css/landing.scss'
import React, {Component} from 'react'
import Skeleton from 'react-loading-skeleton';
import '../../../assets/css/get-audio.scss'
import axios from 'axios';
import moment from 'moment';
import {Link, useParams} from "react-router-dom";
import { MusicalNoteOutline, PlayOutline } from 'react-ionicons'

export default class GetAudio extends Component {
    constructor(props) {
        super(props);

        this.state = {
            audios: [],
            currentAudio: [],
            currentAudioTime: 0,
            isLoading: true,
            isNotPart: !this.props.userId && !this.props.groupId && !this.props.searchQuery && !this.props.subscriber,
            isFetching: false
        };
    }


    getTime (audio) {
        if (moment(moment(Date.now()).diff(moment(audio.createdAt))).format('HH:MM:SS') >= '01:00:00' && moment(moment(Date.now()).diff(moment(audio.createdAt))).format('HH:MM:SS') < '02:00:00') {
            return 'An hour ago'
        } else if (moment(moment(Date.now()).diff(moment(audio.createdAt))).format('HH:MM:SS') >= '02:00:00' && moment(moment(Date.now()).diff(moment(audio.createdAt))).format('HH:MM:SS') < '10:00:00') {
            return moment(moment(Date.now()).diff(moment(audio.createdAt))).format('H') + ' hours ago'
        } else if (moment(moment(Date.now()).diff(moment(audio.createdAt))).format('HH:MM:SS') >= '10:00:00' && moment(moment(Date.now()).diff(moment(audio.createdAt))).format('HH:MM:SS') < '24:00:00') {
            return moment(moment(Date.now()).diff(moment(audio.createdAt))).format('H') + ' hours ago'
        } else if (moment(moment(Date.now()).diff(moment(audio.createdAt))).format('DD') >= '1' && moment(moment(Date.now()).diff(moment(audio.createdAt))).format('DD') < '2') {
            return 'A day ago'
        } else if (moment(moment(Date.now()).diff(moment(audio.createdAt))).format('DD') >= '2' && moment(moment(Date.now()).diff(moment(audio.createdAt))).format('DD') < '10') {
            return moment(moment(Date.now()).diff(moment(audio.createdAt))).format('D') + ' days ago'
        } else if (moment(moment(Date.now()).diff(moment(audio.createdAt))).format('DD') >= '10' && moment(moment(Date.now()).diff(moment(audio.createdAt))).format('DD') < '31') {
            return moment(moment(Date.now()).diff(moment(audio.createdAt))).format('DD') + ' days ago'
        } else if (moment(moment(Date.now()).diff(moment(audio.createdAt))).format('MM') >= '1' && moment(moment(Date.now()).diff(moment(audio.createdAt))).format('MM') < '2') {
            return 'A month ago'
        } else if (moment(moment(Date.now()).diff(moment(audio.createdAt))).format('MM') >= '2' && moment(moment(Date.now()).diff(moment(audio.createdAt))).format('MM') < '10') {
            return moment(moment(Date.now()).diff(moment(audio.createdAt))).format('M') + ' months ago'
        } else if (moment(moment(Date.now()).diff(moment(audio.createdAt))).format('MM') >= '10' && moment(moment(Date.now()).diff(moment(audio.createdAt))).format('MM') < '12') {
            return moment(moment(Date.now()).diff(moment(audio.createdAt))).format('MM') + ' months ago'
        } else if (moment(moment(Date.now()).diff(moment(audio.createdAt))).format('YYY') >= '1' && moment(moment(Date.now()).diff(moment(audio.createdAt))).format('YYY') < '2') {
            return 'A year ago'
        } else if (moment(moment(Date.now()).diff(moment(audio.createdAt))).format('YYY') >= '2' && moment(moment(Date.now()).diff(moment(audio.createdAt))).format('YYY') < '10') {
            return moment(moment(Date.now()).diff(moment(audio.createdAt))).format('Y') + ' years ago'
        } else if (moment(moment(Date.now()).diff(moment(audio.createdAt))).format('YYY') >= '10' && moment(moment(Date.now()).diff(moment(audio.createdAt))).format('YYY') < '100') {
            return moment(moment(Date.now()).diff(moment(audio.createdAt))).format('YY') + ' years ago'
        } else if (moment(moment(Date.now()).diff(moment(audio.createdAt))).format('YYY') >= '100' && moment(moment(Date.now()).diff(moment(audio.createdAt))).format('YYY') < '1000') {
            return moment(moment(Date.now()).diff(moment(audio.createdAt))).format('YYY') + ' years ago'
        }

        return moment(moment(Date.now()).diff(moment(audio.createdAt))).format('YYY')
    }

    componentDidMount() {
        if (this.state.isNotPart) {
            axios.get('http://localhost:5000/api/audio/get')
                .then(response => {
                    if (response.data.success) {
                        this.setState({audios: response.data.audio})
                        this.setState({isLoading: false})
                    } else {
                        alert('Failed to get Audios')
                    }
                }).catch(e => {
                    console.log(e)
            })
        } else if (this.props.searchQuery) {
            axios.get('http://localhost:5000/api/audio/get/search', {headers: {searchQuery: this.props.searchQuery}})
                .then(response => {
                    if (response.data.success) {
                        this.setState({audios: response.data.audio})
                        this.setState({isLoading: false})
                    } else {
                        alert('Failed to get Audios')
                    }
                })
        } else if (this.props.subscriber) {
            axios.get('http://localhost:5000/api/audio/get/subscribed', {headers: {subscriber: this.props.subscriber}})
                .then(response => {
                    if (response.data.success) {
                        this.setState({audios: response.data.audio})
                        this.setState({isLoading: false})
                    } else {
                        alert('Failed to get Audios')
                    }
                })
        }
    }

    componentDidUpdate(prevProps) {
        if (this.props.userId) {
            if (this.props.userId !== prevProps.userId) {
                axios.post('http://localhost:5000/api/audio/get/user', this.props.userId)
                    .then(response => {
                        if (response.data.success) {
                            this.setState( {audios: response.data.audio})
                            this.setState( {isLoading: false})
                        } else {
                            alert('Failed to get Audios')
                        }
                    })
            }
        } else if (this.props.groupId) {
            if (this.props.groupId !== prevProps.groupId) {
                axios.post('http://localhost:5000/api/audio/get/group', this.props.groupId)
                    .then(response => {
                        if (response.data.success) {
                            this.setState( {audios: response.data.audio})
                            this.setState( {isLoading: false})
                        } else {
                            alert('Failed to get Audios')
                        }
                    })
            }
        } else if (this.props.searchQuery) {
            if (this.props.searchQuery !== prevProps.searchQuery) {
                axios.get('http://localhost:5000/api/audio/get/search', {headers: {searchQuery: this.props.searchQuery}})
                    .then(response => {
                        if (response.data.success) {
                            this.setState({audios: response.data.audio})
                            this.setState({isLoading: false})
                        } else {
                            alert('Failed to get Audios')
                        }
                    })
            }
        } else if (this.props.subscriber) {
            if (this.props.subscriber !== prevProps.subscriber) {
                console.log('Subscriber')
                axios.get('http://localhost:5000/api/audio/get/subscribed', {headers: {subscriber: this.props.subscriber}})
                    .then(response => {
                        if (response.data.success) {
                            this.setState({audios: response.data.audio})
                            this.setState({isLoading: false})
                        } else {
                            alert('Failed to get Audios')
                        }
                    })
            }
        }
    }

    render() {
        if (!this.state.isLoading) {
            const renderCards = this.state.audios.map((audio, index) => {

                return <div className="audio-card audio">
                    <div>
                        <Link to={'/audio/' + audio._id}>
                            <div className="audio-title" style={{
                                color: this.props.theme === 'light' ? 'rgb(64 64 64)' : '#e4e4e4'
                            }}><PlayOutline color={this.props.theme === 'light' ? 'rgb(64 64 64)' : '#e4e4e4'}/>{audio.title}</div>
                        </Link>
                    </div>
                    <div className="info"
                    style={{
                        borderTop: `1px solid ${this.props.theme === 'light' ? '#e4e4e4' : 'rgb(84 84 84)'}`
                    }}>
                        { audio.writer && this.state.isNotPart ? <Link className="user" to={'/source/' + audio.writer._id + '/user'}>
                            <img src={audio.writer.image}/>{audio.writer.name}
                        </Link> : ''}
                        { audio.group && this.state.isNotPart ? <Link className="user" to={'/source/' + audio.group._id + '/group'}>
                            <img src={audio.group.image}/>{audio.group.name}
                        </Link> : ''}
                        <div style={{
                            color: this.props.theme === 'light' ? 'rgb(84 84 84)' : 'rgb(84 84 84)'
                        }}><MusicalNoteOutline/> {audio.listened} listened</div>
                        <div className="date" style={{
                            color: this.props.theme === 'light' ? 'rgb(84 84 84)' : 'rgb(84 84 84)'
                        }}> {this.getTime(audio)} </div>
                    </div>
                </div>
            })

            return (
                <div className="recommended">
                    {renderCards}
                </div>
            )
        } else {
            return (
                <div className="recommended">
                    <div className="audio-card">
                        <div className="skeleton">
                            { this.props.userId ? '' : <Skeleton className="avatar" circle={true} height={35} width={35} />}
                            <Skeleton width={100}/>
                        </div>
                        <Skeleton count={2}/>
                    </div>
                    <div className="audio-card">
                        <div className="skeleton">
                            { this.props.userId ? '' : <Skeleton className="avatar" circle={true} height={35} width={35} />}
                            <Skeleton width={100}/>
                        </div>
                        <Skeleton count={2}/>
                    </div>
                    <div className="audio-card">
                        <div className="skeleton">
                            { this.props.userId ? '' : <Skeleton className="avatar" circle={true} height={35} width={35} />}
                            <Skeleton width={100}/>
                        </div>
                        <Skeleton count={2}/>
                    </div>
                </div>
            )
        }

    }

}

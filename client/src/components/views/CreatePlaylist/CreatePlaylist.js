import '../../../assets/css/upload.scss'
import React, { useState, useEffect } from 'react'
import axios from 'axios';
import { useSelector } from "react-redux";
import Skeleton from "react-loading-skeleton";
import {Link, withRouter} from "react-router-dom";
import { BoxLoading } from 'react-loadingg';
import {ChevronBackOutline, ChevronForwardOutline, LinkOutline, Send} from 'react-ionicons'
const crossIcon = require('../../../assets/images/cross.svg')
const checkMarkIcon = require('../../../assets/images/check-mark.svg')

const Private = [
    { value: 0, label: 'Private' },
    { value: 1, label: 'Public' },
    { value: 2, label: 'Only By Link' },
]

function CreatePlaylist(props) {
    const [audio, setAudio] = useState([]);
    const removeAudio = indexToRemove => {
        setAudio([...audio.filter((_, index) => index !== indexToRemove)]);
    };
    const addAudio = event => {
        if (event.target.value !== "") {
            setAudio([...audio, event.target.value]);
            event.target.value = "";
        }
    };
    const user = useSelector(state => state.user);

    const [title, setTitle] = useState("");
    const [description, setDescription] = useState("");
    const [privacy, setPrivacy] = useState(0)
    const [file, setFile] = useState("")
    const [isLoading, setIsLoading] = useState(true)
    const [isSubmitLoading, setIsSubmitLoading] = useState(false)
    const [groups, setGroups] = useState([])
    const [active, setActive] = useState('me')
    const [isForGroup, setIsForGroup] = useState(false)
    const [secretLink, setSecretLink] = useState('')
    const [errMessage, setErrMessage] = useState('')
    const [page, setPage] = useState(1)
    const [audioCount, setAudioCount] = useState(1)
    const link = 'http://localhost:3000/audio/secret/' + secretLink

    function generateRandom(length) {
        var result = '';
        var characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
        var charactersLength = characters.length;
        for ( var i = 0; i < length; i++ ) {
            result += characters.charAt(Math.floor(Math.random() * charactersLength));
        }
        return result;
    }

    const handleChangeTitle = (event) => {
        setTitle(event.currentTarget.value)
    }

    const handleChangeDescription = (event) => {
        setDescription(event.currentTarget.value)
    }

    const handleChangeOne = (event) => {
        setPrivacy(event.currentTarget.value)
        if (event.currentTarget.value == 2) {
            setSecretLink(generateRandom(20))
        } else {
            setSecretLink('')
        }
    }

    const handleFile = (event) => {
        setFile(event.target.files[0])
    }

    const handleActive = (active, isForGroup) => {
        setActive(active)
        setIsForGroup(isForGroup)
    }

    const onSubmit = (event) => {
        event.preventDefault();
        const data = new FormData();

        if (user.userData && !user.userData.isAuth) {
            return alert('Please Log in First')
        }

        if (title === "" || description === "" || file === "") {
            return alert('Please first fill all the fields')
        }

        setIsSubmitLoading(true)
        data.append('file', file);
        data.append('title', title);
        data.append('description', description);
        data.append('writer', localStorage.getItem('userId'));
        data.append('group', active);
        data.append('isForGroup', isForGroup);
        data.append('privacy', privacy);
        data.append('secretLink', secretLink);

        axios.post('api/audio/upload', data).then(response => {
            console.log(response)
            if (response.data.success) {
                setIsSubmitLoading(false)
                props.history.push('/')
            } else {
                setIsSubmitLoading(false)
                setErrMessage('Failed to create audio!')
            }
        }).catch(e => {
            console.log(e)
        })
    }

    const groupList = groups.map(group => {
        return (
            <button className={active === group.group._id ? 'active-group' : 'inactive-group'}
                    onClick={event => handleActive(group.group ? group.group._id : '', true)} style={{marginLeft: '1vw'}}
            >
                {group.group ? group.group.name : ''}
            </button>
        )
    });

    const copyLink = () => {
        navigator.clipboard.writeText(link)
    }

    const goBack = () => {
        setPage(page - 1)
    }

    const goForward = () => {
        setPage(page + 1)
    }

    useEffect(() => {
        axios.post('/api/group/getByUser/upload', {user: localStorage.getItem('userId')}).then(response => {
            if (response.data.success) {
                setGroups(response.data.groupsToUser);
                setIsLoading(false)
            }
        })
    }, [])

    if (!isSubmitLoading) {
        return (
            <div style={{maxWidth: '700px', margin: '5vh 25vw'}}>
                {errMessage.length > 0 ? <div className="error">
                    {errMessage}
                </div> : ''}
                {isLoading ? <div className="skeleton">
                        <Skeleton width={100}/> <Skeleton width={100} style={{marginLeft: '1vw'}}/>
                    </div>
                    : <div>
                        <button className={active === 'me' ? 'active-group' : 'inactive-group'}
                                onClick={event => handleActive('me', false)}
                        >
                            My profile
                        </button>
                        {groupList}
                    </div>}
                <div style={{textAlign: 'center', marginBottom: '5vh', marginTop: '5vh'}}>
                    <h4>Create playlist</h4>
                </div>


                <div onSubmit={onSubmit} className="form">
                    {page === 1 ? <>
                        <div style={{display: 'flex', justifyContent: 'space-between', width: '60vw'}}>
                            <input onChange={handleFile} accept="audio/mpeg3" name="audio" type="file" id="customFile"
                                   style={{display: 'none'}}/>
                            <label htmlFor="customFile" className="upload">
                                {file ? <>
                                    <div><img width="80" src={checkMarkIcon} alt=""/></div>
                                    Audio uploaded successfully
                                </> : <>
                                    <div>+</div>
                                    Upload Audio</>}
                            </label>
                        </div>

                        <input
                            onChange={handleChangeTitle}
                            value={title}
                            className="input"
                            placeholder="Title"
                        />
                        <button
                            className="button"
                            onClick={() => goForward()}
                        >
                            Next <ChevronForwardOutline style={{marginTop: 1}}/>
                        </button>
                    </> : ''}
                    {page === 2 ? <>

                        <textarea
                            onChange={handleChangeDescription}
                            value={description}
                            className="textarea"
                            placeholder="Description"
                            rows="7"
                        />
                        {secretLink.length ? <div className="secret-link">
                            Your current link: <div>
                            <Link to={link}>{link}</Link>
                            <LinkOutline onClick={() => copyLink()}/>
                        </div></div> : ''}
                        <select
                            className="select"
                            onChange={handleChangeOne}
                        >
                            {Private.map((item) => (
                                <option key={item.value} value={item.value}>{item.label}</option>
                            ))}
                        </select>
                        <div style={{display: 'flex'}}>
                            <button
                                className="button"
                                onClick={() => goBack()}
                            >
                                Previous <ChevronBackOutline style={{marginTop: 1}}/>
                            </button>
                            <button
                                className="button"
                                onClick={() => goForward()}
                            >
                                Next <ChevronForwardOutline style={{marginTop: 1}}/>
                            </button>
                        </div>
                    </> : ''}
                    {page === 3 ? <>
                        <ul className="audio-input">
                            {audio.map((tag, index) => (
                                <li key={index}>
                                    <div>{tag}</div>
                                    <div
                                        onClick={() => removeAudio(index)}
                                        className="delete"
                                    >
                                        <img src={crossIcon} alt=""/>
                                    </div>
                                </li>
                            ))}
                            <input
                                type="text"
                                onKeyUp={event => event.key === "Enter" ? addAudio(event) : null}
                                placeholder="Press enter to add audio"
                                maxLength="60"
                            />
                        </ul>
                        <div style={{display: 'flex'}}>
                            <button
                                className="button"
                                onClick={() => goBack()}
                            >
                                Previous <ChevronBackOutline style={{marginTop: 1}}/>
                            </button>
                            <button
                                className="button"
                                type="submit"
                                onClick={() => onSubmit}
                            >
                                Submit <Send/>
                            </button>
                        </div>
                    </> : ''}

                </div>
            </div>
        )
    } else {
        return (
            <BoxLoading CircleLoading={true}/>
        )
    }
}

export default withRouter(CreatePlaylist)

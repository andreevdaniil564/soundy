import '../../../assets/css/subscribed.scss'
import React, { useEffect, useState } from 'react'
import GetAudio from "../GetAudio/GetAudio";

function SubscriptionPage() {
    return (
        <div style={{width: '60vw'}}>
            <div className="subscribed-audio">
                <h2> Subscribed Audios </h2>
                <hr />
            </div>

        <div>
            <GetAudio subscriber={localStorage.getItem('userId')} />
        </div>
    </div>
    )
}

export default SubscriptionPage

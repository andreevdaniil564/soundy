import React, {useEffect, useState} from 'react';
import axios from "axios";
import Skeleton from "react-loading-skeleton";
import {Link, useParams, useLocation, withRouter} from "react-router-dom";
import '../../../assets/css/my-audios.scss'
import '../../../assets/css/landing.scss'
import moment from "moment";
import AudioPlayer from "../AudioPlayer";
import { MusicalNoteOutline } from 'react-ionicons'

const MyAudios = (props) => {
    const user = localStorage.getItem('userId')
    const [audios, setAudios] = useState([])
    const [groups, setGroups] = useState([])
    const [isLoading, setIsLoading] = useState(true)
    const [currentAudioTime, setCurrentAudioTime] = useState(0)
    const [active, setActive] = useState('my')
    const location = useLocation()

    const handleVolume = (e) => {
        localStorage.setItem('volume', e.target.volume)
    }

    const group = {
        id: useParams().id
    }

    const handleAudio = (e) => {
        console.log(e)
    }

    const handleCurrentTime = (e) => {
        setCurrentAudioTime(e.target.currentTime)
    }

    useEffect(() => {
        axios.post('/api/group/getByUser/upload', {user: user})
            .then(response => {
                if (response.data.success) {
                    setGroups(response.data.groupsToUser)
                    setIsLoading(false)
                } else {
                    alert('Failed to get audio Info')
                }
            })
    }, [])

    useEffect(() => {
        setIsLoading(true)
        if (group.id === 'my') {
            setActive(group.id)
            axios.post('/api/audio/get/user', {id: user})
                .then(response => {
                    if (response.data.success) {
                        setAudios(response.data.audio)
                        setIsLoading(false)
                    } else {
                        alert('Failed to get audio Info')
                    }
                })
        } else {
            setActive(group.id)
            axios.post('/api/audio/get/group', {id: group.id})
                .then(response => {
                    if (response.data.success) {
                        setAudios(response.data.audio)
                        setIsLoading(false)
                    } else {
                        alert('Failed to get audio Info')
                    }
                })
        }
    }, [location])

    const groupList = groups.map(group => {
        return (
            <Link className={active === group.group._id ? 'active-group' : 'inactive-group'}
                    to={'/my-audios/' + group.group._id}
                    style={{marginLeft: '1vw'}}
            >
                {group.group ? group.group.name : ''}
            </Link>
        )
    });

    const renderCards = audios.map((audio) => {
        return <div className="audio-card audio">
            <div>
                <Link to={'/audio/' + audio._id}>
                    <div className="audio-title"  style={{
                        color: props.theme === 'light' ? 'rgb(64 64 64)' : '#e4e4e4'
                    }}>{audio.title}</div>
                </Link>
            </div>
            <AudioPlayer autoPlay={false} audio={audio} />
            <div className="info">
                { audio.writer ? <Link className="user" to={'/source/' + audio.writer._id + '/user'}>
                    <img src={audio.writer.image}/>{audio.writer.name}
                </Link> : ''}
                { audio.group ? <Link className="user" to={'/source/' + audio.group._id + '/group'}>
                    <img src={audio.group.image}/>{audio.group.name}
                </Link> : ''}
                <div style={{
                    color: props.theme === 'light' ? 'rgb(64 64 64)' : '#e4e4e4'
                }}><MusicalNoteOutline/> {audio.listened} listened</div>
                <div className="date"  style={{
                    color: props.theme === 'light' ? 'rgb(64 64 64)' : '#e4e4e4'
                }}> {moment(audio.createdAt).format("MMM Do YY")} </div>
            </div>
        </div>
    })

    return (
        <div style={{width: '60vw', padding: '5vh 0'}}>
            <Link className={active === 'my' ? 'active-group' : 'inactive-group'} to="/my-audios/my">
                My profile
            </Link>
            {groupList}

            <h2 style={{marginTop: '3vh', color: props.theme === 'light' ? 'rgb(64 64 64)' : '#e4e4e4'}}>Audios </h2>

            {isLoading ? <> <div className="audio-card">
                <div className="skeleton">
                    <Skeleton width={100}/>
                </div>
                <Skeleton count={2}/>
            </div>
                <div className="audio-card">
                    <div className="skeleton">
                        <Skeleton width={100}/>
                    </div>
                    <Skeleton count={2}/>
                </div>
                <div className="audio-card">
                    <div className="skeleton">
                       <Skeleton width={100}/>
                    </div>
                    <Skeleton count={2}/>
                </div></> : audios.length > 0 ? renderCards : <h3>No audios found</h3> }
        </div>
    );

};

export default withRouter(MyAudios);

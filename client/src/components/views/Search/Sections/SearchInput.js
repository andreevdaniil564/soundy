import React, {useEffect, useState} from 'react';
import axios from "axios";
import {Link, useLocation} from "react-router-dom";
import {SearchOutline} from "react-ionicons";

const SearchInput = (props) => {
    const [searchList, setSearchList] = useState([])
    const [searchQuery, setSearchQuery] = useState([])
    const [isSuggestions, setIsSuggestions] = useState(false)

    const location = useLocation()

    const handleSearch = (e) => {
        setSearchQuery(e.target.value)
        axios.get('/api/audio/get/search-suggestions', {headers: {searchQuery: e.target.value}})
            .then(response => {
                if (response.data.success) {
                    setIsSuggestions(true)
                    setSearchList(response.data.audio)
                } else {
                    alert('Failed to get audio list')
                }
            })
    }

    useEffect(() => {
        setIsSuggestions(false)
    } ,[location])

    const suggestions = searchList.map(search => (
            <li>
                <Link to={'/audio-search/' + search.title}>{search.title}</Link>
            </li>
        ))

    return (
        <>
          <div className="search-form">
            <input
                onChange={handleSearch}
                type="text"
                className="search-input"
                placeholder="Search for"
                style={{
                    border: `1px solid ${props.theme === 'light' ? '#e3e3e3' : '#0a0707'}`,
                    background: props.theme === 'light' ? '#e3e3e3' : '#0a0707',
                    color: props.theme === 'light' ? '#0a0707' : '#e3e3e3',
                }}
            />
            <button
                style={{
                    background: props.theme === 'light' ? '#e3e3e3' : '#0a0707'
                }}
            >
                <SearchOutline color={props.theme === 'light' ? '#0a0707' : '#e3e3e3'} />
            </button>
          </div>
            {searchQuery.length > 0 && isSuggestions ?
                <ul
                className="suggestions-list"
                style={{
                    background: props.theme === 'light' ? '#fcfcfc' : '#101416'
                }}
            >{suggestions}</ul> : '' }
        </>
    );
};

export default SearchInput;

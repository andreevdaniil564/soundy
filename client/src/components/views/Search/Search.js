import '../../../assets/css/search.scss'
import React from 'react';
import SearchInput from "./Sections/SearchInput";

const Search = (props) => {
    return (
        <div>
            <SearchInput theme={props.theme}/>
        </div>
    );
};

export default Search;

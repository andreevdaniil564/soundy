import '../../../assets/css/main.scss'
import React from 'react'
import GetAudio from "../GetAudio/GetAudio";
import {Link} from "react-router-dom";
import {RadioOutline} from "react-ionicons";

function LandingPage(props) {
    return (
        <div className="main-container">
            <Link to="/start-stream" className="stream-button">Start stream <RadioOutline/></Link>
            <GetAudio theme={props.theme}/>
        </div>
    )
}

export default LandingPage

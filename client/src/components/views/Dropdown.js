import React from "react";
import {Link} from "react-router-dom";
import '../../assets/css/dropdown.scss'

export default class Dropdown extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            open: false,
            selected: this.props.initial || -1,
            active: false
        };
    }

    toggleDropdown() {
        this.setState({
            active: !this.state.active
        });
    }

    handleClick(i) {
        this.setState({
            selected: i
        });
    }

    renderOptions() {
        if (!this.props.options) {
            return;
        }

        return this.props.options.map((option, i) => {
            return (
                <li
                    onClick={evt => this.handleClick(i)}
                    key={i}
                >
                    <Link to={option.link}>{option.name}</Link>
                </li>
            );
        });
    }

    render() {
        return (
            <>
                <div
                    onClick={() => this.toggleDropdown()}
                    className="dropdown_toggle dropdown_list-item"
                >
                    {this.props.title}
                </div>
                <div className="dropdown">
                    {this.state.active ? <ul className="dropdown_list-item--active">
                        {this.renderOptions()}
                    </ul> : ''}
                </div>
            </>
        );
    }
}


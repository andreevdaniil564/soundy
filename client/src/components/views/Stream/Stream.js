import React, {useState} from 'react';
import axios from "axios";
import {Link, useHistory} from "react-router-dom";
import {RocketOutline} from "react-ionicons";

const Private = [
    { value: 0, label: 'Private' },
    { value: 1, label: 'Public' },
    { value: 2, label: 'Only by link' },
]

const Stream = (props) => {
    const [title, setTitle] = useState('')
    const [description, setDescription] = useState('')
    const [privacy, setPrivacy] = useState(0)
    const [secretLink, setSecretLink] = useState('')
    const history = useHistory()

    const join = () => {
        const data = {
            title,
            description,
            privacy,
            secretLink,
            userId: localStorage.getItem('userId')
        }

        axios.post(`http://localhost:7000/stream/publish`, data).then(res => {
            if (res.data.success) {
                localStorage.setItem('streamName', res.data.stream.title)
                localStorage.setItem('streamDescription', res.data.stream.description)
                localStorage.setItem('streamKey', res.data.stream.secretKey)
                localStorage.setItem('streamKey', res.data.stream.secretKey)
                history.push('/audio-live/' + res.data.stream._id)
            } else {
                alert(res.data.err.message)
            }
        })
    }

    const handleChangeTitle = (event) => {
        setTitle(event.currentTarget.value)
    }

    const handleChangeDescription = (event) => {
        setDescription(event.currentTarget.value)
    }

    const handleChangeOne = (event) => {
        setPrivacy(event.currentTarget.value)
        if (event.currentTarget.value == 2) {
            setSecretLink(generateRandom(20))
        } else {
            setSecretLink('')
        }
    }

    function generateRandom(length) {
        var result = '';
        var characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
        var charactersLength = characters.length;
        for ( var i = 0; i < length; i++ ) {
            result += characters.charAt(Math.floor(Math.random() * charactersLength));
        }
        return result;
    }


    return (
        <div style={{margin: '0 20vw'}}>
            <input
                onChange={handleChangeTitle}
                value={title}
                className="input"
                placeholder="Name"
            />
            <br/><br/>
            <textarea
                onChange={handleChangeDescription}
                value={description}
                className="textarea"
                placeholder="Description"
            />
            <br/><br/>

            <select
                className="select"
                onChange={handleChangeOne}
            >
                {Private.map((item, index) => (
                    <option key={index} value={item.value}>{item.label}</option>
                ))}
            </select>
            <br/><br/>
            {secretLink.length ? <div className="secret-link">
                Your current link: <Link
                to={'http://localhost:3000/stream/secret/' + secretLink}>{'http://localhost:3000/stream/secret/' + secretLink}</Link>
            </div> : ''}

            <button onClick={() => join()} className="button">Start <RocketOutline/></button>
        </div>
    );
};

export default Stream;

import '../../../assets/css/library.scss'
import React, {useEffect, useState} from 'react';
import axios from "axios";
import moment from "moment";
import {Link} from "react-router-dom";
import {ChevronBackOutline, MusicalNoteOutline, PlayOutline} from "react-ionicons";
import Skeleton from "react-loading-skeleton";

const Liked = () => {
    const [audios, setAudios] = useState([])
    const [isLoading, setIsLoading] = useState(true)
    const userId = localStorage.getItem('userId')


    const getTime = (audio) => {
        if (moment(moment(Date.now()).diff(moment(audio.createdAt))).format('HH:MM:SS') >= '01:00:00' && moment(moment(Date.now()).diff(moment(audio.createdAt))).format('HH:MM:SS') < '02:00:00') {
            return 'An hour ago'
        } else if (moment(moment(Date.now()).diff(moment(audio.createdAt))).format('HH:MM:SS') >= '02:00:00' && moment(moment(Date.now()).diff(moment(audio.createdAt))).format('HH:MM:SS') < '10:00:00') {
            return moment(moment(Date.now()).diff(moment(audio.createdAt))).format('H') + ' hours ago'
        } else if (moment(moment(Date.now()).diff(moment(audio.createdAt))).format('HH:MM:SS') >= '10:00:00' && moment(moment(Date.now()).diff(moment(audio.createdAt))).format('HH:MM:SS') < '24:00:00') {
            return moment(moment(Date.now()).diff(moment(audio.createdAt))).format('H') + ' hours ago'
        } else if (moment(moment(Date.now()).diff(moment(audio.createdAt))).format('DD') >= '1' && moment(moment(Date.now()).diff(moment(audio.createdAt))).format('DD') < '2') {
            return 'A day ago'
        } else if (moment(moment(Date.now()).diff(moment(audio.createdAt))).format('DD') >= '2' && moment(moment(Date.now()).diff(moment(audio.createdAt))).format('DD') < '10') {
            return moment(moment(Date.now()).diff(moment(audio.createdAt))).format('D') + ' days ago'
        } else if (moment(moment(Date.now()).diff(moment(audio.createdAt))).format('DD') >= '10' && moment(moment(Date.now()).diff(moment(audio.createdAt))).format('DD') < '31') {
            return moment(moment(Date.now()).diff(moment(audio.createdAt))).format('DD') + ' days ago'
        } else if (moment(moment(Date.now()).diff(moment(audio.createdAt))).format('MM') >= '1' && moment(moment(Date.now()).diff(moment(audio.createdAt))).format('MM') < '2') {
            return 'A month ago'
        } else if (moment(moment(Date.now()).diff(moment(audio.createdAt))).format('MM') >= '2' && moment(moment(Date.now()).diff(moment(audio.createdAt))).format('MM') < '10') {
            return moment(moment(Date.now()).diff(moment(audio.createdAt))).format('M') + ' months ago'
        } else if (moment(moment(Date.now()).diff(moment(audio.createdAt))).format('MM') >= '10' && moment(moment(Date.now()).diff(moment(audio.createdAt))).format('MM') < '12') {
            return moment(moment(Date.now()).diff(moment(audio.createdAt))).format('MM') + ' months ago'
        } else if (moment(moment(Date.now()).diff(moment(audio.createdAt))).format('YYY') >= '1' && moment(moment(Date.now()).diff(moment(audio.createdAt))).format('YYY') < '2') {
            return 'A year ago'
        } else if (moment(moment(Date.now()).diff(moment(audio.createdAt))).format('YYY') >= '2' && moment(moment(Date.now()).diff(moment(audio.createdAt))).format('YYY') < '10') {
            return moment(moment(Date.now()).diff(moment(audio.createdAt))).format('Y') + ' years ago'
        } else if (moment(moment(Date.now()).diff(moment(audio.createdAt))).format('YYY') >= '10' && moment(moment(Date.now()).diff(moment(audio.createdAt))).format('YYY') < '100') {
            return moment(moment(Date.now()).diff(moment(audio.createdAt))).format('YY') + ' years ago'
        } else if (moment(moment(Date.now()).diff(moment(audio.createdAt))).format('YYY') >= '100' && moment(moment(Date.now()).diff(moment(audio.createdAt))).format('YYY') < '1000') {
            return moment(moment(Date.now()).diff(moment(audio.createdAt))).format('YYY') + ' years ago'
        }

        return moment(moment(Date.now()).diff(moment(audio.createdAt))).format('YYY')
    }

    useEffect(() => {
        axios.post('/api/audio/liked', {userId})
            .then(response => {
                if (response.data.success) {
                    setAudios(response.data.audio)
                    setIsLoading(false)
                } else {
                    alert('Failed to get audio')
                }
            })
    }, [])

    const renderCards = audios.map((audio, index) => {

        return <div className="audio-card audio">

            <div>
                <Link to={'/audio/' + audio.audio._id}>
                    <div className="audio-title"><PlayOutline/>{audio.audio ? audio.audio.title : ''}</div>
                </Link>
            </div>
        </div>
    })

    return (
        <div style={{width: '60vw'}}>
            <Link to="/library" className="history-link"><ChevronBackOutline /> Back</Link>
            <h2 className="history-title">Liked</h2>
            {isLoading ?  <div><div className="audio-card">
                    <div className="skeleton">
                        <Skeleton width={100}/>
                    </div>
                    <Skeleton count={2}/>
                </div>
                <div className="audio-card">
                    <div className="skeleton">
                        <Skeleton width={100}/>
                    </div>
                    <Skeleton count={2}/>
                </div>
                <div className="audio-card">
                    <div className="skeleton">
                        <Skeleton width={100}/>
                    </div>
                    <Skeleton count={2}/>
                </div>
                </div> : renderCards}
        </div>
    );
};

export default Liked;

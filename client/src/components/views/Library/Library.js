import '../../../assets/css/library.scss'
import React, {useEffect, useState} from 'react';
import {TimeOutline, ChevronForward, HeartOutline, BookmarksOutline} from "react-ionicons";
import {Link} from "react-router-dom";

const libraryLinks = [
    {name: 'History', link: '/library/history', icon: <TimeOutline/>},
    {name: 'Liked', link: '/library/liked', icon: <HeartOutline/>},
    {name: 'Playlists', link: '/library/playlists', icon: <BookmarksOutline/>},
]

const Library = () => {
    const renderOptions = libraryLinks.map((option) => {
        return (
            <li>
                <Link to={option.link}>
                    <span>{option.icon}{option.name}</span>
                    <span className="chevron"><ChevronForward/></span>
                </Link>
            </li>
        )
    })

    return (
        <div className="library-container">
            <ul className="library-list">
                {renderOptions}
            </ul>
        </div>
    );
};

export default Library;

import React, { useEffect, useState } from 'react'
import axios from 'axios';
import {Link, useParams} from "react-router-dom";
import '../../../assets/css/upload.scss'
import '../../../assets/css/detail.scss'
import '../../../assets/css/subscribe.scss'
import moment from "moment";
import GetAudio from "../GetAudio/GetAudio";
import Skeleton from "react-loading-skeleton";

export default function Source () {
    const [source, setSource] = useState([])
    const [loading, setLoading] = useState(true)
    const [isSubscribed, setIsSubscribed] = useState(false)
    const [subscribers, setSubscribers] = useState(0)

    const variable = {
        id: useParams().id
    }

    const type = {
        type: useParams().type
    }

    const subscribeData = {
        userTo: useParams().id,
        userFrom: localStorage.getItem('userId')
    }

    console.log(type.type)

    useEffect(() => {
        if (type.type === 'user') {
            axios.post('/api/users/get', variable)
                .then(response => {
                    if (response.data.success) {
                        setSource(response.data.user)
                        setSubscribers(response.data.subscribers)
                        setLoading(false)
                    } else {
                        alert('Failed to get audio Info')
                    }
                })
        } else if (type.type === 'group') {
            axios.post('/api/group/get-specific', variable)
                .then(response => {
                    if (response.data.success) {
                        setSource(response.data.group)
                        setSubscribers(response.data.subscribers)
                        setLoading(false)
                    } else {
                        alert('Failed to get audio Info')
                    }
                })
        }

        axios.post('/api/subscribe/subscribed', subscribeData)
            .then(response => {
                if (response.data.success) {
                    setIsSubscribed(response.data.subscribed)
                } else {
                    console.log('Failed')
                }
            })
    }, [])

    const subscribe = () => {
        axios.post('/api/subscribe/subscribe', subscribeData)
            .then(response => {
                if (response.data.success) {
                    setSubscribers(subscribers + 1)
                    console.log(subscribers)
                    setIsSubscribed(response.data.subscribed)
                } else {
                    alert('Failed to get audio Info')
                }
            })
    }

    const unsubscribe = () => {
        axios.post('/api/subscribe/unSubscribe', subscribeData)
            .then(response => {
                if (response.data.success) {
                    setSubscribers(subscribers - 1)
                    setIsSubscribed(response.data.subscribed)
                } else {
                    alert('Failed to get audio Info')
                }
            })
    }

    if (!loading) {
        return (
            <div className="post-page" style={{width: '100%'}}>
                <div className="audio-card subscription">
                    <img src={source.backgroundImage} alt="" className="bg-image"/>
                    <div className={`info ${source.backgroundImage ? 'community image' : 'community'}`}>
                        <div className="user">{source.image ? <img src={source.image}/> : ''} {source.name} </div>
                        <div>{subscribers} listeners</div>
                        <button onClick={isSubscribed ? unsubscribe : subscribe} className={isSubscribed ? 'subscribed' : 'subscribe'}>
                            { isSubscribed ? 'Unfollow' : 'Follow' }
                        </button>
                        <div className="date"> {moment(source.createdAt).format("MMM Do YYYY")} </div>
                    </div>
                </div>

                {type.type === 'user' ? <GetAudio userId={variable}/> : <GetAudio groupId={variable}/>}
            </div>
        )
    } else {
        return (
            <div>
                <div style={{margin: '5vh 20vw'}}>
                    <div className="audio-card">
                        <div className="skeleton">
                            <Skeleton className="avatar" circle={true} height={35} width={35} />
                            <Skeleton width={100}/>
                        </div>
                        <Skeleton count={2}/>
                    </div>
                </div>
                {type.type === 'user' ? <GetAudio userId={variable}/> : <GetAudio groupId={variable}/>}
            </div>
        )
    }

}

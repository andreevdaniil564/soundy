import '../../../assets/css/stream.scss'
import React, { useEffect, useState, useRef } from 'react';
import axios from "axios";
import {useHistory} from "react-router-dom";

function StreamDetail() {
    const history = useHistory()

    const endStream = () => {
        axios.post(`http://localhost:7000/stream/finish`, {streamId: localStorage.getItem('streamId')}).then(res => {
            if (res.data.success) {
                localStorage.removeItem('streamName')
                localStorage.removeItem('streamDescription')
                localStorage.removeItem('streamKey')
                history.push('/')
            } else {
                alert(res.data.err.message)
            }
        })
    }
    return (
        <div className="stream-container">
            <h4>{localStorage.getItem('streamName')}</h4>
            <p>{localStorage.getItem('streamDescription')}</p>
            <h5>{localStorage.getItem('streamKey')}</h5>
            <button onClick={() => endStream()} className="stream-btn">End stream</button>
        </div>
    );
}

export default StreamDetail;

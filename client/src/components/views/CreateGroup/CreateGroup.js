import '../../../assets/css/upload.scss'
import React, { useState, useEffect } from 'react'
import axios from 'axios';
import { useSelector } from "react-redux";
import {Link, withRouter} from "react-router-dom";
import { CommonLoading } from 'react-loadingg';
import {Send} from "react-ionicons";
const checkMarkIcon = require('../../../assets/images/check-mark.svg')

const Private = [
    { value: 0, label: 'Private' },
    { value: 1, label: 'Public' },
    { value: 2, label: 'Only by link' },
]

function CreateGroup(props) {
    const user = useSelector(state => state.user);

    const [title, setTitle] = useState("");
    const [description, setDescription] = useState("");
    const [privacy, setPrivacy] = useState(0)
    const [file, setFile] = useState("")
    const [isSubmitLoading, setIsSubmitLoading] = useState(false)
    const [secretLink, setSecretLink] = useState('')

    const handleChangeTitle = (event) => {
        setTitle(event.currentTarget.value)
    }

    const handleFile = (event) => {
        const img = new Image()
        img.src = URL.createObjectURL(event.target.files[0])
        img.onload = function () {
            console.log(this.width + ' ' + this.height)
            if (this.width != 1300 || this.height != 300) {
                alert('Invalid file size. Width must be 1300px. Height must be 300px.')
                setFile('')
            }
        }
        setFile(event.target.files[0])
    }

    function generateRandom(length) {
        var result = '';
        var characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
        var charactersLength = characters.length;
        for ( var i = 0; i < length; i++ ) {
            result += characters.charAt(Math.floor(Math.random() * charactersLength));
        }
        return result;
    }

    const handleChangeDescription = (event) => {
        setDescription(event.currentTarget.value)
    }

    const handleChangeOne = (event) => {
        setPrivacy(event.currentTarget.value)
        if (event.currentTarget.value == 2) {
            setSecretLink(generateRandom(20))
        } else {
            setSecretLink('')
        }
    }

    const onSubmit = (event) => {

        event.preventDefault();

        const data = new FormData();

        if (user.userData && !user.userData.isAuth) {
            return alert('Please Log in First')
        }

        if (title === "" || description === "") {
            return alert('Please first fill all the fields')
        }

        setIsSubmitLoading(true)
        data.append('file', file);
        data.append('name', title);
        data.append('description', description);
        data.append('user', localStorage.getItem('userId'));
        data.append('privacy', privacy);

        axios.post('/api/group/create', data).then(response => {
            if (response.data.success) {
                setIsSubmitLoading(false)
                alert('Group created Successfully')
                props.history.push('/')
            } else {
                alert('Failed to create group')
            }
        })
    }

    if (!isSubmitLoading) {
        return (
            <div style={{maxWidth: '700px'}}>
                <div style={{textAlign: 'center', marginBottom: '2vh'}}>
                    <h4>Create Group</h4>
                </div>

                <form onSubmit={onSubmit}>
                    <div style={{display: 'flex', justifyContent: 'space-between', width: '60vw'}}>
                        <input onChange={handleFile} accept="image/jpeg, image/png" name="audio" type="file" id="customFile"
                               style={{display: 'none'}}/>
                        <label htmlFor="customFile" className="upload">
                            {file ? <>
                                <div><img width="80" src={checkMarkIcon} alt=""/></div>
                                Image uploaded successfully
                            </> : <>
                                <div>+</div>
                                Upload Image
                                <p>*Width must be 1300px. Height must be 300px.</p>
                            </>}
                        </label>
                    </div>
                    <input
                        onChange={handleChangeTitle}
                        value={title}
                        className="input"
                        placeholder="Name"
                    />
                    <br/><br/>
                    <textarea
                        onChange={handleChangeDescription}
                        value={description}
                        className="textarea"
                        placeholder="Description"
                    />
                    <br/><br/>

                    <select
                        className="select"
                        onChange={handleChangeOne}
                    >
                        {Private.map((item, index) => (
                            <option key={index} value={item.value}>{item.label}</option>
                        ))}
                    </select>
                    <br/><br/>
                    {secretLink.length ? <div className="secret-link">
                        Your current link: <Link
                        to={'http://localhost:3000/audio/secret/' + secretLink}>{'http://localhost:3000/audio/secret/' + secretLink}</Link>
                    </div> : ''}

                    <button
                        className="button"
                        type="primary"
                        onClick={onSubmit}
                    >
                        Submit <Send/>
                    </button>

                </form>
            </div>
        )
    } else {
        return (
            <CommonLoading/>
        )
    }
}

export default withRouter(CreateGroup)

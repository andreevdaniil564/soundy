import React from 'react';
import {useParams} from "react-router-dom";
import GetAudio from "../GetAudio/GetAudio";

const AudioSearch = () => {
    const query = useParams().query

    return (
        <>
           <GetAudio searchQuery={query} />
        </>
    );
};

export default AudioSearch;

import React, {useEffect, useState} from 'react';
import RightMenu from './Sections/RightMenu';
import Drawer from "../Drawer";
import '../../../assets/css/navbar.scss';
import {Link} from "react-router-dom";
import {
    AlbumsOutline,
    Albums,
    SettingsOutline,
    Settings,
    Menu,
    Search as SearchIcon,
    MusicalNoteOutline,
    MusicalNote,
    HeadsetOutline,
    Headset,
    PersonAddOutline,
    PersonAdd,
    Mic,
    MicOutline,
    DuplicateOutline, Duplicate
} from 'react-ionicons'
import Search from "../Search/Search";
const Logo = require('../../../assets/images/Logo.svg');

const accountOptions = [
  {name: 'My audios', link: '/my-audios/my', icon: <MusicalNoteOutline/>, activeIcon: <MusicalNote/>},
  {name: 'Subscriptions', link: '/subscription', icon: <HeadsetOutline/>, activeIcon: <Headset/>},
  {name: 'Create group', link: '/group-create', icon: <PersonAddOutline/>, activeIcon: <PersonAdd/>},
  {name: 'Upload audio', link: '/audio-upload', icon: <MicOutline/>, activeIcon: <Mic/>},
  {name: 'Create Playlist', link: '/playlist-create', icon: <DuplicateOutline/>, activeIcon: <Duplicate/>},
  {name: 'Library', link: '/library', icon: <AlbumsOutline/>, activeIcon: <Albums/>},
  {name: 'Settings', link: '/settings', icon: <SettingsOutline/>, activeIcon: <Settings/>},
]

function NavBar(props) {
    const [isSearchOpened, setIsSearchOpened] = useState(false)

    useEffect(() => {
        if (props.windowWidth > 767) {
            setIsSearchOpened(true)
        } else if (props.windowWidth < 767) {
            setIsSearchOpened(false)
        }
    }, [props.windowWidth])

    return (
    <nav className="menu" style={{
        position: 'fixed',
        zIndex: 10,
        width: '100%',
        boxShadow: `0 0 30px ${props.theme === 'light' ? '#f3f1f1' : '#0a0f11'}`,
        background: props.theme === 'light' ? '#ffffff' : '#101416',
        borderBottom: props.theme === 'light' ? '#ffffff' : '#000000'
    }}>
        <div className={`menu__left ${isSearchOpened ? 'search-opened' : ''}`}>
            {localStorage.getItem('userId') != null ? <button onClick={() => props.toggleDrawer()}>
                <Menu color={props.theme === 'light' ? '#101416' : '#ffffff'}/>
            </button> : ''}
            {!isSearchOpened || props.windowWidth > 767 ? <Link to="/">
                <img src={Logo} alt="Logo" style={{
                    width: '30px',
                    marginTop: '-5px',
                }}/>
            </Link> : ''}
        </div>
      <div className="menu__container">
        <div className="menu_left">

            {isSearchOpened ? <Search theme={props.theme}/> : ''}


            {props.windowWidth < 767 ? <button className="search-open-button" onClick={() => setIsSearchOpened(!isSearchOpened)}>
                <SearchIcon color={props.theme === 'light' ? '#707070' : '#ffffff'}/>
            </button> : ''}
        </div>
          <div className="menu_right">
              <RightMenu mode="horizontal" accountOptions={accountOptions} checkAuth={props.checkAuth}/>
          </div>
          {localStorage.getItem('userId') ? <Drawer theme={props.theme} accountOptions={accountOptions} mode={props.mode}/> : ''}
      </div>
    </nav>
  )
}

export default NavBar

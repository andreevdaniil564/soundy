import '../../../../assets/css/right-menu.scss'
import React from 'react';
import axios from 'axios';
import { USER_SERVER } from '../../../Config';
import { withRouter } from 'react-router-dom';
import { useSelector } from "react-redux";
import { Link } from "react-router-dom";

function RightMenu(props) {
  const user = useSelector(state => state.user)

  const logoutHandler = () => {
    axios.get(`${USER_SERVER}/logout`).then(response => {
      if (response.status === 200) {
        props.history.push("/login");
      } else {
        alert('Log Out Failed')
      }
    });
    localStorage.removeItem('userId')
    window.location.reload()
  };

  if (!localStorage.getItem('userId')) {
    return (
      <ul className="right-list">
        <li>
            <Link to="/login">Signin</Link>
        </li>
        <li>
          <Link to="/register">Signup</Link>
        </li>
      </ul>
    )
  } else {
    return (
      <ul className="right-list">
        <li>
          <a onClick={logoutHandler}>Logout</a>
        </li>
      </ul>
    )
  }
}

export default withRouter(RightMenu);


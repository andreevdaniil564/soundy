import '../../../assets/css/upload.scss'
import '../../../assets/css/settings.scss'
import React, {useEffect, useState} from 'react';
import axios from "axios";
import Spinner from 'react-spinner-material';
import {Link} from "react-router-dom";
import {CheckmarkCircle, Send} from "react-ionicons";

const Settings = (props) => {
    const [user, setUser] = useState(null)
    const userId = localStorage.getItem('userId')
    const [isLoading, setIsLoading] = useState(true)
    const [name, setName] = useState('')
    const [description, setDescription] = useState('')
    const [file, setFile] = useState('')
    const [errMessage, setErrMessage] = useState('')

    useEffect(() => {
        axios.post('/api/users/get-current', {userId}).then((res) => {
            setUser(res.data.user)
            setName(res.data.user.name)
            setDescription(res.data.user.description)
            setIsLoading(false)
        })
    }, [])


    const handleChangeName = (event) => {
        setName(event.currentTarget.value)
    }

    const handleChangeDescription = (event) => {
        setDescription(event.currentTarget.value)
    }

    const handleFile = (event) => {
        setFile(event.target.files[0])
    }

    const onSubmit = (event) => {
        event.preventDefault();
        const data = new FormData();

        if (user.userData && !user.userData.isAuth) {
            return alert('Please Log in First')
        }

        if (name === "" || description === "") {
            return alert('Please first fill all the fields')
        }

        setIsLoading(true)
        data.append('file', file);
        data.append('name', name);
        data.append('description', description);
        data.append('theme', props.theme);

        axios.post('api/audio/upload', data).then(response => {
            console.log(response)
            if (response.data.success) {
                setIsLoading(false)
                props.history.push('/')
            } else {
                setIsLoading(false)
                setErrMessage('Failed to create audio!')
            }
        }).catch(e => {
            console.log(e)
        })
    }


    const specificColor = (theme) => {
        if (theme == 'orange') {
            return '#F76C6C'
        } else if (theme == 'white') {
            return '#ffffff'
        } else if (theme == 'blue') {
            return '#659DBD'
        } else if (theme == 'dark') {
            return '#272727'
        }
    }

    return (
        <div>
            <h2 className="settings-name">Settings</h2>
            {isLoading ?
                <Spinner size={120} spinnerColor={"#333"} spinnerWidth={2} visible={true} />:
                <form onSubmit={onSubmit} className="form" encType="multipart">
                    <div style={{display: 'flex', justifyContent: 'space-between', width: '60vw'}}>
                        <input onChange={handleFile} accept="image/png, image/jpeg, image/jpg" name="audio" type="file" id="customFile" style={{display: 'none'}}/>
                        <label htmlFor="customFile" className="upload">
                            {file ? <>
                                <div><CheckmarkCircle alt=""/></div>
                                Avatar uploaded successfully
                            </> : <><div>+</div>
                                Upload Avatar</>}
                        </label>
                    </div>

                    <input
                        onChange={handleChangeName}
                        value={name}
                        className="input"
                        placeholder="Name"
                    />
                    <textarea
                        onChange={handleChangeDescription}
                        value={description}
                        className="textarea"
                        placeholder="Description"
                        rows="7"
                    />

                    <button
                        className="button"
                        type="primary"
                        onClick={onSubmit}
                    >
                        Submit <Send/>
                    </button>

                </form>
            }
        </div>
    );
};

export default Settings;

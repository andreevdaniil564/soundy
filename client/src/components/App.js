import React, {Component, useEffect, useState} from 'react';
import {BrowserRouter, Route, Switch, useHistory, useLocation} from "react-router-dom";
import LandingPage from "./views/LandingPage/LandingPage.js";
import LoginPage from "./views/LoginPage/LoginPage.js";
import RegisterPage from "./views/RegisterPage/RegisterPage.js";
import NavBar from "./views/NavBar/NavBar";
import UploadAudioPage from "./views/UploadAudioPage/UploadAudioPage"
import DetailAudioPage from "./views/DetailAudioPage/DetailAudioPage"
import SubscriptionPage from "./views/SubscriptionPage/SubscriptionPage"
import Source from "./views/Source/Source";
import CreateGroup from "./views/CreateGroup/CreateGroup";
import MyAudios from "./views/MyAudios/MyAudios";
import ProtectedRoute from "./ProtectedRoute";
import StreamDetail from "./views/StreamDetail/StreamDetail";
import AudioSearch from "./views/AudioSearch/AudioSearch";
import History from "./views/Library/History";
import Library from "./views/Library/Library";
import Liked from "./views/Library/Liked";
import CreatePlaylist from "./views/CreatePlaylist/CreatePlaylist";
import Stream from "./views/Stream/Stream";
import Settings from "./views/Settings/Settings";
import {Moon, Sunny} from "react-ionicons";

class App extends Component {
  constructor(props) {
    super(props);

    this.state = {
        mode: localStorage.getItem('mode') == null ? 'mini' : localStorage.getItem('mode'),
        windowWidth: window.innerWidth,
        isAuth: false,
        theme: localStorage.getItem('theme')
    }
  }

  toggleDrawer = () => {
    if (this.state.mode == 'maxi') {
      this.setState({mode: 'mini'})
      localStorage.setItem('mode', 'mini')
    } else if (this.state.mode == 'mini') {
      this.setState({mode: 'maxi'})
      localStorage.setItem('mode', 'maxi')
    }
  }

    getInitialState() {
        return window.innerWidth
    }

    handleResize = (e) => {
        this.setState({windowWidth: window.innerWidth});
    }

    componentDidMount() {
        window.addEventListener('resize', this.handleResize);
    }

    componentWillUnmount() {
        window.removeEventListener('resize', this.handleResize);
    }

    changeTheme = (theme) => {
        localStorage.setItem('theme', theme)
        this.setState({theme: theme})
    }

  render() {
      return (
      <BrowserRouter>
          <div>
            <NavBar theme={this.state.theme} mode={this.state.mode} toggleDrawer={() => this.toggleDrawer()} windowWidth={this.state.windowWidth}/>
              <div className="theme-button">
                  {this.state.theme == 'dark' ?
                      <Sunny color='#ffffff' onClick={() => this.changeTheme('light')}/> :
                      <Moon color='#707070' onClick={() => this.changeTheme('dark')}/>}
              </div>
            <div style={{
                paddingTop: '75px',
                minHeight: '100vh',
                minWidth: '100vw',
                overflow: 'hidden',
                display: 'flex',
                flexDirection: 'column',
                alignItems: 'center',
                background: this.state.theme === 'light' ? 'white' : '#222629'
            }}
                 className={`${this.state.mode != null && this.state.mode === `mini` ? `mini-main` : ''} ${this.state.mode != null && this.state.mode === `maxi` ? `maxi-main` : ''}`}
                 onClick={() => this.setState({mode: 'mini'})}
            >
                <Route exact path="/">
                    <LandingPage theme={this.state.theme}/>
                </Route>
                <Route exact path="/login" component={LoginPage} />
                <Route exact path="/register" component={RegisterPage} />
                <ProtectedRoute exact path="/audio-upload" component={UploadAudioPage} />
                <ProtectedRoute exact path="/group-create" component={CreateGroup} />
                <ProtectedRoute exact path="/playlist-create" component={CreatePlaylist} />
                <Route exact path="/audio/:id" component={DetailAudioPage}/>
                <Route exact path="/audio/:secretLink/:secret" component={DetailAudioPage} />
                <Route exact path="/source/:id/:type" component={Source} />
                <Route exact path="/audio-live/:id" component={StreamDetail} />
                <Route exact path="/audio-search/:query" component={AudioSearch} />
                <ProtectedRoute exact path="/subscription" component={SubscriptionPage} />
                <ProtectedRoute exact path="/start-stream" component={Stream} />
                <ProtectedRoute exact path="/library" component={Library} />
                <ProtectedRoute exact path="/library/history" component={History} />
                <ProtectedRoute exact path="/settings" component={Settings} />
                <ProtectedRoute exact path="/library/liked" component={Liked} />
                <ProtectedRoute exact path="/my-audios/:id">
                    <MyAudios theme={this.state.theme}/>
                </ProtectedRoute>
            </div>
          </div>
      </BrowserRouter>
    );
  }
}

export default App

const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const groupSchema = mongoose.Schema({
    name: {
        type: String,
        required: true
    },
    description: {
        type: String
    },
    privacy: {
        type: Number
    },
    status: {
        type: Number,
        default: 9
    },
}, { timestamps: true })


const Group = mongoose.model('Group', groupSchema);

module.exports = { Group }

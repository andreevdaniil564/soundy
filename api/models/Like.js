const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const likeSchema = mongoose.Schema({
   user: {
       type: Schema.Types.ObjectId,
       ref: 'User'
   },
   comment: {
       type: Schema.Types.ObjectId,
       ref: 'Comment'
   },
   audio: {
       type: Schema.Types.ObjectId,
       ref: 'Audio'
   }

}, { timestamps: true })


const Like = mongoose.model('Like', likeSchema);

module.exports = { Like }

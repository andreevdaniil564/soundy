const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const groupToUserSchema = mongoose.Schema({
    audio: {
        type: Schema.Types.ObjectId,
        ref: 'User',
        required: true
    },
    playlist: {
        type: Schema.Types.ObjectId,
        ref: 'Group',
        required: true
    }
}, { timestamps: true })


const GroupToUser = mongoose.model('GroupToUser', groupToUserSchema);

module.exports = { GroupToUser }

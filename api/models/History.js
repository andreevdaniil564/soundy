const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const historySchema = mongoose.Schema({
    user: {
        type: Schema.Types.ObjectId,
        ref: 'User',
        required: true
    },
    audio: {
        type: Schema.Types.ObjectId,
        ref: 'Audio',
        required: true
    }

}, { timestamps: true })


const History = mongoose.model('History', historySchema);

module.exports = { History }

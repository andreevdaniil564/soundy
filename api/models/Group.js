const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const groupSchema = mongoose.Schema({
    name: {
        type: String,
        required: true
    },
    description: {
        type: String
    },
    backgroundImage: {
        type: String
    },
    privacy: {
        type: Number
    },
    status: {
        type: Number,
        default: 9
    },
    tag: {
        type: Schema.Types.ObjectId,
        ref: 'Tag'
    },
}, { timestamps: true })


const Group = mongoose.model('Group', groupSchema);

module.exports = { Group }

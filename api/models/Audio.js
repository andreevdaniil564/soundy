const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const audioSchema = mongoose.Schema({
    writer: {
        type: Schema.Types.ObjectId,
        ref: 'User'
    },
    group: {
        type: Schema.Types.ObjectId,
        ref: 'Group'
    },
    title: {
        type:String,
        maxlength:50,
        required: true
    },
    listened: {
        type: Number,
        default: 0
    },
    tags: {
      type: Array
    },
    time: {
        type: Number,
        default: 0
    },
    description: {
        type: String,
        required: true
    },
    privacy: {
        type: Number,
        required: true
    },
    secretLink: {
        type: String,
    },
    filePath: {
        type: String,
        required: true
    },
    audioSpeech: {
        type: String,
    }
}, { timestamps: true })


const Audio = mongoose.model('Audio', audioSchema);

module.exports = { Audio }

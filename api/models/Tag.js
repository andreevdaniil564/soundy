const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const tagSchema = mongoose.Schema({
    title: {
        type: String
    }
})


const Tag = mongoose.model('Tag', tagSchema);

module.exports = { Tag }

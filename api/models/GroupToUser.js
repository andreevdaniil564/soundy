const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const groupToUserSchema = mongoose.Schema({
    user: {
        type: Schema.Types.ObjectId,
        ref: 'User',
        required: true
    },
    group: {
        type: Schema.Types.ObjectId,
        ref: 'Group',
        required: true
    },
    isAdmin: {
        type: Boolean,
        default: false
    },
    isModerator: {
        type: Boolean,
        default: false
    },
    isMember: {
        type: Boolean,
        default: true
    },
}, { timestamps: true })


const GroupToUser = mongoose.model('GroupToUser', groupToUserSchema);

module.exports = { GroupToUser }

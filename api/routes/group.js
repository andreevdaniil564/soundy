const express = require('express');
const { Group } = require('../models/Group')
const { Subscriber } = require('../models/Subscriber')
const { GroupToUser } = require('../models/GroupToUser')
const router = express.Router();
const AWS = require('aws-sdk');
const upload = require('multer')()

const s3 = new AWS.S3({
    accessKeyId: process.env.accessKeyId,
    secretAccessKey: process.env.secretAccessKey
});

router.post("/create", upload.single('file'), (req, res) => {
    const params = {
        Bucket: process.env.bucket,
        Key: Date.now() + req.file.originalname,
        Body: req.file.buffer,
        ACL: "public-read"
    };
    s3.upload(params, function(s3Err, data) {
        if (s3Err) throw s3Err
        const group = new Group({name: req.body.name, description: req.body.description, privacy: req.body.privacy, backgroundImage: data.Location})

        group.save((err, group) => {
            if (err) return res.status(400).json({success: false, err})

            const groupToUser = new GroupToUser({
                user: req.body.user,
                group: group._id,
                isAdmin: true,
                isModerator: true,
            })

            groupToUser.save((err) => {
                if (err) return res.json({success: false, err})
                return res.status(200).json({success: true})
            })
        })
    })
})

router.post("/getByUser/upload", (req, res) => {
    GroupToUser.find({'user': req.body.user, 'isModerator': true}).populate('group').then((groupsToUser) => {
        if (groupsToUser) {
            return res.status(200).json({ success: true, groupsToUser })
        } else {
            return res.status(400).json({ success: false })
        }
    })

})

router.post("/get-specific", (req, res) => {
    Group.findOne({'_id': req.body.id}).then((group) => {
        if (!group) return res.status(400).json({ success: false })

        Subscriber.find({"userTo": req.body.id})
            .then(subscribers => {
                return res.status(200).json({ success: true, group, subscribers: subscribers.length })
            })
    })

})

router.post("/member", (req, res) => {

    const groupToUser = new GroupToUser(req.body)
    const sentFrom = req.body.sentFrom

    if (req.body.isAdmin || req.body.isModerator) {
         GroupToUser.findOne({'group': req.body.groupId, 'user': sentFrom}).then(group => {
             if (!group.isAdmin) {
                 return res.status(400).json({ success: false })
             }

             groupToUser.save((err) => {
                 if (err) return res.json({ success: false, err })
                 return res.status(200).json({ success: true })
             })
             return res.status(200).json({ success: true })
         })
    }

    groupToUser.save((err) => {
        if (err) return res.json({ success: false, err })
        return res.status(200).json({ success: true })
    })
})

module.exports = router;

const express = require('express');
const router = express.Router();
const { Tag } = require("../models/Tag");
const { Subscriber } = require("../models/Subscriber");
const { auth } = require("../middleware/auth");

router.get("/get", (req, res) => {
    Tag.find({})
        .exec((err, tags) => {
            if (err) return res.status(400).send(err)
            res.status(200).json({ success: true, tags })
        })
});

router.post("/add", (req, res) => {
    const tag = new Tag(req.body)

    tag.save((err) => {
        if (err) return res.json({ success: false, err })

        return res.status(200).json({ success: true })
    })
});


router.post("/get-specific", (req, res) => {
    Tag.findOne({ "_id" : req.body.tagId })
    .populate('writer')
    .exec((err, audio) => {
        if(err) return res.status(400).send(err);
        res.status(200).json({ success: true, audio })
    })
});

router.post("/getSubscriptionVideos", (req, res) => {
    Subscriber.find({ 'userFrom': req.body.userFrom })
    .exec((err, subscribers)=> {
        if(err) return res.status(400).send(err);

        let subscribedUser = [];

        subscribers.map((subscriber, i)=> {
            subscribedUser.push(subscriber.userTo)
        })


        Audio.find({ writer: { $in: subscribedUser }})
            .populate('writer')
            .exec((err, videos) => {
                if(err) return res.status(400).send(err);
                res.status(200).json({ success: true, videos })
            })
    })
});

module.exports = router;

const express = require('express');
const router = express.Router();


const { Subscriber } = require("../models/Subscriber");

const { auth } = require("../middleware/auth");

router.post("/subscribeNumber", (req, res) => {

    Subscriber.find({ "userTo": req.body.userTo })
    .exec((err, subscribe) => {
        if(err) return res.status(400).send(err)

        res.status(200).json({ success: true, subscribeNumber: subscribe.length  })
    })

});


router.post("/subscribed", (req, res) => {

    Subscriber.find({ "userTo": req.body.userTo , "userFrom": req.body.userFrom })
    .exec((err, subscribe) => {
        if(err) return res.status(400).send(err)

        let result = false;
        if(subscribe.length !== 0) {
            result = true
        }

        res.status(200).json({ success: true, subscribed: result  })
    })

});



router.post("/subscribe", (req, res) => {

    const subscribe = new Subscriber(req.body);
    console.log(req.body)

    Subscriber.findOne({userTo: req.body.userTo, userFrom: req.body.userFrom}, (err, subscription) => {
        if (subscription) {
            return res.status(400).json({ success: false })
        }

        subscribe.save((err) => {
            if(err) return res.json({ success: false, err })
            return res.status(200).json({ success: true, subscribed: true })
        })
    })

});


router.post("/unSubscribe", (req, res) => {

    Subscriber.findOneAndDelete({ userTo: req.body.userTo, userFrom: req.body.userFrom })
        .exec((err, doc)=>{
            if(err) return res.status(400).json({ success: false, err});
            res.status(200).json({ success: true, doc })
        })
});



module.exports = router;

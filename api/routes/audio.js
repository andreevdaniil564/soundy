const express = require('express');
const router = express.Router();
const { Audio } = require("../models/Audio");
const { GroupToUser } = require("../models/GroupToUser");
const { Subscriber } = require("../models/Subscriber");
const { auth } = require("../middleware/auth");
const AWS = require('aws-sdk');
const upload = require('multer')()
const redis = require('redis')
const {Like} = require('../models/Like')
const {History} = require('../models/History')
const axios = require('axios');

const client = redis.createClient({
    host: '127.0.0.1',
    port: 6379
})

const s3 = new AWS.S3({
    accessKeyId: process.env.accessKeyId,
    secretAccessKey: process.env.secretAccessKey
});


router.post("/get/user", (req, res) => {
    Audio.find({ "writer" : req.body.id })
        .exec((err, audio) => {
            if(err) return res.status(400).send(err);

             GroupToUser.find({'user': req.body.id})
                .populate('group')
                .then(groups => {
                    res.status(200).json({ success: true, audio, groups })
                })
        })
});


router.post("/like", (req, res) => {
    const audioOrComment = req.body.isAudio ? {audioId: req.body.audioId} : { commentId: req.body.commentId }
    Like.findOne({ "userId" : req.body.userId }, audioOrComment)
        .exec((err, like) => {
            if(like) return res.status(400).send('AlreadyExists');

            const commentData = {
                userId: req.body.userId,
                commentId: req.body.commentId
            }

            const audioData = {
                userId: req.body.userId,
                audioId: req.body.audioId,
            }

            const newLike = new Like(req.body.isAudio ? audioData : commentData)
            newLike.save().then((newLike) => {
                res.status(200).json({ success: true, newLike })
            })

        })
});

router.post("/get-like-count", (req, res) => {
    client.get("like-count", (err, response) => {
        if (response) {
            const likeCount = JSON.parse(response)
            res.status(200).json({ success: true, likesCount: likeCount.length })
        } else {
            const audioOrComment = req.body.isAudio ? {audioId: req.body.audioId} : { commentId: req.body.commentId }
            Like.find(audioOrComment)
                .exec((err, likes) => {
                    if(err) return res.status(400).send('AlreadyExists');

                    client.set("like-count", JSON.stringify(likes), 'EX', 5 * 60)
                    res.status(200).json({ success: true, likesCount: likes.length })
                })
        }
    });
});

router.post("/get-like", (req, res) => {
    client.get("liked", (err, response) => {
        if (response) {
            const like = JSON.parse(response)
            res.status(200).json({ success: true, like: like == null ? false : true })
        } else {
            const audioOrComment = req.body.isAudio ? {"userId" : req.body.userId, audioId: req.body.audioId} : {"userId" : req.body.userId, commentId: req.body.commentId }

            Like.findOne(audioOrComment)
                .exec((err, like) => {
                    if(err) return res.status(400).send(err);

                    client.set("liked", JSON.stringify(like), 'EX', 2.5 * 60)
                    res.status(200).json({ success: true, like: like == null ? false : true })
                })
        }
    });
});

router.post("/unlike", (req, res) => {
    const audioOrComment = req.body.isAudio ? {userId : req.body.userId, audioId: req.body.audioId} : { userId : req.body.userId, commentId: req.body.commentId }
    Like.deleteOne(audioOrComment, (err) => {
        if(err) return res.status(400).send(err);

        res.status(200).json({ success: true })
    })
});

router.get("/get/search", (req, res) => {
    Audio.find().or([{ 'title': { $regex: req.headers.searchquery, $options: 'i' }},
        { 'description': { $regex: req.headers.searchquery, $options: 'i' }}, { 'tags': { $regex: req.headers.searchquery, $options: 'i' }}])
        .exec((err, audio) => {
            if(err) return res.status(400).send(err);

            res.status(200).json({ success: true, audio })
        })
});

router.get("/get/subscribed", (req, res) => {
    client.get("audio-subscribed", (err, response) => {
        if (response) {
            const audio = JSON.parse(response)
            res.status(200).json({ success: true, audio })
        } else {
            Subscriber.find({ 'userFrom': req.headers.subscriber })
                .exec((err, subscribers)=> {
                    if(err) return res.status(400).send(err);

                    let subscribedUser = [];

                    subscribers.map((subscriber, i)=> {
                        subscribedUser.push(subscriber.userTo)
                    })


                    Audio.find({ $or:[{ writer: { $in: subscribedUser }}, {group: {$in: subscribedUser}}]} )
                        .populate('writer group')
                        .exec((err, audio) => {
                            if(err) return res.status(400).send(err);

                            client.set("audio-subscribed", JSON.stringify(audio), 'EX', 5 * 60);
                            res.status(200).json({ success: true, audio })
                        })
                })
        }
    });
});

router.get("/get/search-suggestions", (req, res) => {
    Audio.find().or([{ 'title': { $regex: req.headers.searchquery, $options: 'i' }},
        { 'description': { $regex: req.headers.searchquery, $options: 'i' }}])
        .exec((err, audio) => {
            if(err) return res.status(400).send(err);

            res.status(200).json({ success: true, audio })
        })
});

router.post("/get/group", (req, res) => {
    Audio.find({ "group" : req.body.id })
        .exec((err, audio) => {
            if(err) return res.status(400).send(err);
            res.status(200).json({ success: true, audio })
        })
});

router.get("/get", (req, res) => {
    client.get("audio", (err, response) => {
        if (response) {
            const audio = JSON.parse(response)
            res.status(200).json({ success: true, audio })
        } else {
            Audio.find({'privacy': 1})
                .populate('writer group')
                .exec((err, audio) => {
                    if (err) return res.status(400).send(err)

                    client.set("audio", JSON.stringify(audio), 'EX', 5 * 60);
                    res.status(200).json({ success: true, audio })
                })
        }
    });


});

router.post("/history", (req, res) => {
    client.get("history-audio", (err, response) => {
        if (response) {
            const audio = JSON.parse(response)
            res.status(200).send({ success: true, audio })
        } else {
            History.find({user: req.body.userId})
                .populate('audio')
                .exec((err, audio) => {
                    if (err) return res.status(400).send(err)

                    client.set("history-audio", JSON.stringify(audio), 'EX', 2.5 * 60);
                    res.status(200).json({ success: true, audio })
                })
        }
    });


});

router.post("/liked", (req, res) => {
    client.get("liked-audio", (err, response) => {
        if (response) {
            const audio = JSON.parse(response)
            res.status(200).json({ success: true, audio })
        }
    });

    Like.find({user: req.body.userId})
        .populate('audio')
        .exec((err, audio) => {
            if (err) return res.status(400).send(err)

            client.set("liked-audio", JSON.stringify(audio), 'EX', 2.5 * 60);
            res.status(200).json({ success: true, audio })
        })
});

router.post("/get/secret-link", (req, res) => {
    Audio.findOne({'secretLink': req.body.secretLink})
        .populate('writer group')
        .exec((err, audio) => {
            if (err) return res.status(400).send(err)

            res.status(200).json({ success: true, audio })
        })
});

function generateRandom(length) {
    var result = '';
    var characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    var charactersLength = characters.length;
    for ( var i = 0; i < length; i++ ) {
        result += characters.charAt(Math.floor(Math.random() * charactersLength));
    }
    return result;
}

router.post("/upload", upload.single('file'), (req, res) => {
    const params = {
        Bucket: process.env.bucket,
        Key: Date.now() + req.file.originalname,
        Body: req.file.buffer,
        ACL: "public-read"
    };
    // axios.post('http://0.0.0.0:80/recognize', req.body)
    //     .then(response => {
    //         console.log(response)
    //     })
    //     .catch(error => {
    //         console.log(error.message);
    //     });
    s3.upload(params, function(s3Err, data) {
        if (s3Err) throw s3Err
        const groupData = {
            title: req.body.title,
            description: req.body.title,
            filePath: data.Location,
            group: req.body.group,
            privacy: req.body.privacy,
            secretLink: req.body.secretLink,
            tags: req.body.tags
        }

        const userData = {
            title: req.body.title,
            description: req.body.title,
            filePath: data.Location,
            writer: req.body.writer,
            privacy: req.body.privacy,
            secretLink: req.body.secretLink,
            tags: req.body.tags
        }

        const audio = new Audio(req.body.isForGroup == 'true' ? groupData : userData)

        audio.save((err, audio) => {
            if (err) return res.json({ success: false, err })

            Audio.find({ '_id': audio._id })
                .populate(req.body.isForGroup ? 'group' : 'writer')
                .exec((err, result) => {
                    if (err) return res.json({ success: false, err })
                    return res.status(200).json({ success: true, result })
                })
        })
    });
});


router.post("/get-specific", (req, res) => {
    client.get("specific-audio", (err, response) => {
        const audio = JSON.parse(response)
        if (response && audio._id == req.body.audioId) {
            res.status(200).send({ success: true, audio })
        } else {
            Audio.findOne({ "_id" : req.body.audioId })
            .populate('writer group')
            .exec((err, audio) => {
                if(err) return res.status(400).send(err);

                client.set("specific-audio", JSON.stringify(audio), 'EX', 5 * 60);
                res.status(200).json({ success: true, audio })
            })
        }
    })
});


router.post("/add-listener", (req, res) => {
    Audio.findOne({_id: req.body.audioId }).then((audio) => {
        if (audio === null) return res.status(400).send({success: false});

        Audio.updateOne({ _id : req.body.audioId }, { $inc: { listened: 1 }  }).exec((err) => {
            if (err) return res.status(400).send(err);

            history = new History({audio: req.body.audioId, user: req.body.userId})

            history.save().then((audio) => {
                res.status(200).json({ success: true })
            })
        })
    })

});


router.post("/add-time", (req, res) => {
    Audio.updateOne({ "_id" : req.body.audioId }, { time: req.body.currentTime  }).exec((err) => {
        if (err) return res.status(400).send(err);

        res.status(200).json({ success: true })
    })
});

module.exports = router;

const express = require('express');
const router = express.Router();
const { User } = require("../models/User");
const { Subscriber } = require("../models/Subscriber");
const AWS = require('aws-sdk');
const upload = require('multer')()
const { auth } = require("../middleware/auth");

const s3 = new AWS.S3({
    accessKeyId: process.env.accessKeyId,
    secretAccessKey: process.env.secretAccessKey
});

router.get("/auth", auth, (req, res) => {
    res.status(200).json({
        _id: req.user._id,
        isAdmin: req.user.role === 0 ? false : true,
        isAuth: true,
        email: req.user.email,
        name: req.user.name,
        lastname: req.user.lastname,
        role: req.user.role,
        image: req.user.image,
    });
});

router.post("/register", (req, res) => {

    const user = new User(req.body);

    user.save((err, doc) => {
        if (err) return res.json({ success: false, err });
        return res.status(200).json({
            success: true
        });
    });
});

router.post("/login", (req, res) => {
    User.findOne({ email: req.body.email }, (err, user) => {
        if (!user)
            return res.json({
                loginSuccess: false,
                message: "Auth failed, email not found"
            });

        user.comparePassword(req.body.password, (err, isMatch) => {
            if (!isMatch)
                return res.json({ loginSuccess: false, message: "Wrong password" });

            user.generateToken((err, user) => {
                if (err) return res.status(400).send(err);
                res.cookie("w_authExp", user.tokenExp);
                res
                    .cookie("w_auth", user.token)
                    .status(200)
                    .json({
                        loginSuccess: true, userId: user._id
                    });
            });
        });
    });
});

router.get("/logout", auth, (req, res) => {
    User.findOneAndUpdate({ _id: req.user._id }, { token: "", tokenExp: "" }, (err, doc) => {
        if (err) return res.json({ success: false, err });
        return res.status(200).send({
            success: true
        });
    });
});

router.post("/get", (req, res) => {
    User.findOne({ "_id": req.body.id })
        .exec((err, user) => {
            if(err) return res.status(400).send(err);

            Subscriber.find({"userTo": req.body.id})
                .then(subscribers => {
                    res.status(200).json({ success: true, user, subscribers: subscribers.length  })
                })
        })
});

router.put('/update', upload.single('file'), (req, res) => {
    const params = {
        Bucket: process.env.bucket,
        Key: Date.now() + req.file.originalname,
        Body: req.file.buffer,
        ACL: "public-read"
    };
    s3.upload(params, function(s3Err, data) {
        if (s3Err) throw s3Err

        User.findOneAndUpdate({_id: req.body.userId}, {$set: {
                name: req.body.name,
                image: data.Location,
                theme: req.body.theme,
                password: req.body.password,
        }}).then(() => {
            return res.status(200).json({success: true})
        })
    });
})

router.post('/get-current', (req, res) => {
    User.findOne({_id: req.body.userId}).then(user => {
        return res.status(200).json({success: true, user})
    })
})

module.exports = router;
